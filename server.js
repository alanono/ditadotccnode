var express = require("express");
var bodyParser = require("body-parser");
var mongodb = require("mongodb");
var baseInicial = require("./base-inicial.json");
var ObjectID = mongodb.ObjectID;

var CONTACTS_COLLECTION = "contacts";
var PALAVRAS_COLLECTION = "palavras";
var PARTIDAS_COLLECTION = "partidas";
var RESULTADOS_COLLECTION = "resultados";
var USER_COLLECTION = "usuario";

var app = express();
app.use(bodyParser.json());

// // Create link to Angular build directory
// var distDir = __dirname + "/dist/";
// app.use(express.static(distDir));

// Create a database variable outside of the database connection callback to reuse the connection pool in your app.
var db;

// Connect to the database before starting the application server.
mongodb.MongoClient.connect(process.env.MONGODB_URI || "mongodb://admin:01a10a90@ds153980.mlab.com:53980/heroku_090v135n", function (err, client) {
  if (err) {
    console.log(err);
    process.exit(1);
  }

  // Save database object from the callback for reuse.
  db = client.db();
  console.log("Database connection ready");

  // Initialize the app.
  var server = app.listen(process.env.PORT || 8080, function () {
    var port = server.address().port;
    console.log("App now running on port", port);
  });
});

// Palavras API ROUTES BELOW

// Generic error handler used by all endpoints.
function handleError(res, reason, message, code) {
  console.log("ERROR: " + reason);
  res.status(code || 500).json({"error": message});
}

app.get("/api/start-base", function(req, res) {
  console.log(baseInicial);
  db.collection(PALAVRAS_COLLECTION).insertMany(baseInicial, function(err, docs) {
    if (err) {
      return handleError(res, err.message, "Falha ao iniciar base");
    } else {
      res.status(200).json(docs);
    }
  });
});

app.get("/api/palavras", function(req, res) {
  db.collection(PALAVRAS_COLLECTION).find({}).toArray(function(err, docs) {
    if (err) {
      return handleError(res, err.message, "Falha ao obter palavras");
    } else {
      res.status(200).json(docs);
    }
  });
});

app.post("/api/palavras/buscar", function(req, res) {
  db.collection(PALAVRAS_COLLECTION).find({ "createDate" : { "$gte" : new Date("2013-10-01") } }).toArray(function(err, docs) {
    if (err) {
      return handleError(res, err.message, "Falha ao obter palavras");
    } else {
      res.status(200).json(docs);
    }
  });
});

app.post("/api/login", function(req, res) {
  db.collection(USER_COLLECTION).findOne({ "login" : req.body.login, "senha": req.body.senha }, function(err, doc) {
    if (err) {
      return handleError(res, err.message, "Falha ao obter palavras");
    } else {
      res.status(200).json(doc);
    }
  });
});

app.post("/api/usuario", function(req, res) {
  db.collection(USER_COLLECTION).insertOne(req.body, function(err, doc) {
    if (err) {
      return handleError(res, err.message, "Falha ao criar novo usuario");
    } else {
      res.status(201).json(doc);
    }
  });
});

app.get("/api/palavras/sinc/:data", function(req, res) {
  db.collection(PALAVRAS_COLLECTION).find({ "createDate" : { "$gte" : req.params.data } }).toArray(function(err, docs) {
    if (err) {
      return handleError(res, err.message, "Falha ao obter palavras");
    } else {
      res.status(200).json(docs);
    }
  });
});

app.post("/api/palavras", function(req, res) {
  var novaPalavra = req.body;
  novaPalavra.createDate = new Date();

  if (!req.body.palavra) {
    return handleError(res, "Dados obrigatórios não preenchidos", "Favor informar a palavra", 400);
  }

  db.collection(PALAVRAS_COLLECTION).insertOne(novaPalavra, function(err, doc) {
    if (err) {
      return handleError(res, err.message, "Falha ao criar nova palavra");
    } else {
      res.status(201).json(doc.ops[0]);
    }
  });
});


app.post("/api/partidas", function(req, res) {
  var novaPartida = req.body;
  novaPartida.createDate = new Date();

  console.log(req.body);
  if (!req.body.palavras || !req.body.partidaId) {
    console.log("a");
    return handleError(res, "Dados obrigatórios não preenchidos", "Favor informar a partida", 400);
  }

  db.collection(PARTIDAS_COLLECTION).insertOne(novaPartida, function(err, doc) {
    console.log("b");
    if (err) {
      return handleError(res, err.message, "Falha ao criar nova partida");
    } else {
      console.log("c");
      var newResult = {partidaId: novaPartida.partidaId, prof: novaPartida.prof, type: 0};
      db.collection(RESULTADOS_COLLECTION).insertOne(newResult, function(err, doc2) {
        console.log(doc2);
        if(err){
          console.log("d");
          console.log(err);
          return handleError(res, err, err, 400);
        }
        console.log("e");
        console.log(doc);
        res.status(201).json(doc);
      });
    }
  });
});

app.get("/api/partidas/:id", function(req, res) {
  db.collection(PARTIDAS_COLLECTION).findOne({ "partidaId" : req.params.id }, function(err, doc) {
    if (err) {
      return handleError(res, err.message, "Falha ao obter palavras");
    } else {
      res.status(200).json(doc);
    }
  });
});

app.post("/api/resultados", function(req, res) {
  var novaPartida = req.body;
  novaPartida.createDate = new Date();

  if (!req.body.palavras || !req.body.partidaId || !req.body.resultadoId) {
    console.log("a");
    console.log(req.body);
    return handleError(res, "Dados obrigatórios não preenchidos", "Favor informar a partida", 400);
  }

  db.collection(RESULTADOS_COLLECTION).insertOne(novaPartida, function(err, doc) {
    console.log("b");
    if (err) {
      return handleError(res, err.message, "Falha ao criar nova partida");
    } else {
      console.log("c");
      res.status(201).json(doc);
    }
  });
});

app.get("/api/resultados/:prof/all", function(req, res) {
  db.collection(RESULTADOS_COLLECTION).aggregate([{ $match: {"prof" : req.params.prof }}, {$group: {_id: "$partidaId", acertos: {$sum: "$acertos"}, erros: {$sum: "$erros"}, count: {$sum: 1} }}]).toArray(function(err, docs) {
    if (err) {
      return handleError(res, err.message, "Falha ao obter resultados");
    } else {
      res.status(200).json(docs);
    }
  });
});

app.get("/api/resultados/:prof/:partida", function(req, res) {
  db.collection(RESULTADOS_COLLECTION).find({ "partidaId" : req.params.partida, "type": 1 }).toArray(function(err, docs) {
    if (err) {
      return handleError(res, err.message, "Falha ao obter resultados");
    } else {
      res.status(200).json(docs);
    }
  });
});

app.delete("/api/contacts/:id", function(req, res) {
  db.collection(CONTACTS_COLLECTION).deleteOne({_id: new ObjectID(req.params.id)}, function(err, result) {
    if (err) {
      return handleError(res, err.message, "Failed to delete contact");
    } else {
      res.status(200).json(req.params.id);
    }
  });
});
